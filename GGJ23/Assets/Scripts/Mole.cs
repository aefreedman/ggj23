using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    public class Mole : MonoBehaviour
    {
        private ParticleSystem ps;
        private Animator anim;
        public bool sad = true;

        // Start is called before the first frame update
        void Start()
        {
            ps = GetComponent<ParticleSystem>();
            anim = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            if (sad)
            {
                ps.Play();
                anim.Play("sad");
            } else
            {
                ps.Stop();
                anim.Play("happy");
            }
        }
    }
}
