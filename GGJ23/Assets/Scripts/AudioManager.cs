using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[Serializable]
public class AudioGroup
{
    public AudioClip[] clips;

    public AudioClip GetRandomClip()
    {
        return clips[UnityEngine.Random.Range(0, clips.Length)];
    }
}

public class AudioManager : MonoBehaviour
{
    public class AudioCollision : IEquatable<AudioCollision>
    {
        public float volume;

        private float _timeStamp;
        private Vector3 _position;
        private AudioTag _tag1, _tag2;
        private int _hash; //cache the hash code because it's kind of a lot of calculations to do for ever comparison. easier to just store it, also since the internal data is immutable.

        public int Hash => _hash;
        public AudioTag Tag1 => _tag1;
        public AudioTag Tag2 => _tag2;
        public Vector3 Position => _position;

        public AudioCollision(AudioTag tag1, AudioTag tag2, Vector3 position, float volume)
        {
            _tag1 = tag1;
            _tag2 = tag2;
            _position = position;
            _timeStamp = Time.time;
            _hash = GenerateHash();

            this.volume = volume;
        }

        public override bool Equals(object obj)
        {
            return obj is AudioCollision ? Equals(obj as AudioCollision) : false;
        }

        public bool Equals(AudioCollision otherAudioCollision)
        {
            return this.GetHashCode() == otherAudioCollision.GetHashCode();
        }

        //hash format: time + vector3 + tag1 + tag2   
        private int GenerateHash()
        {
            // int hash = Helper.CombineHashCodes(_timeStamp.GetHashCode(), _position.GetHashCode());
            int hash = _timeStamp.GetHashCode();
            int a = _tag1.GetHashCode();
            int b = _tag2.GetHashCode();
            int subhash = a > b ? Helper.CombineHashCodes(a, b) : Helper.CombineHashCodes(b, a);
            return Helper.CombineHashCodes(hash, subhash);
        }

        public override int GetHashCode()
        {
            return _hash;
        }
    } 

    public static AudioManager Instance;
    [Header("Other Sfx")]
    public AudioGroup root_split;

    [Header("Collision sounds")]
    public AudioGroup tip_prop;
    public AudioGroup tip_nomove;
    public AudioGroup prop_nomove;

    public AudioGroup rootBody_prop;
    public AudioGroup rootBody_nomove;
    public AudioGroup rootBody_tip;

    public AudioMixerGroup mixer_general;
    public AudioMixerGroup mixer_rootbody;
    public AudioMixerGroup mixer_roottip;
    public AudioMixerGroup mixer_sfx;

    private Stack<AudioCollision> stack;
    private List<int> processedList;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        stack = new Stack<AudioCollision>();
        processedList = new List<int>();
    }

    void Update()
    {
        ProcessQueue();
    }

    private bool CollisionIsOfType(AudioCollision entry, AudioTag tag1, AudioTag tag2)
    {
        return (entry.Tag1 == tag1 && entry.Tag2 == tag2) || (entry.Tag1 == tag2 && entry.Tag2 == tag1);
    }

    //todo make this a pool or something.
    private AudioSource GetAudioSource()
    {
        GameObject go = new GameObject();
        var result = go.AddComponent<AudioSource>();
        result.playOnAwake = false;
        return result;
    }

    private void ReturnAudioSourceToPool(AudioSource audioSource)
    {
        Destroy(audioSource.gameObject);
    }

    //audiosource is playing, when it's finished playing, return it to the pool.
    IEnumerator AudioSourceListener(AudioSource audioSource)
    {
        while (audioSource.isPlaying)
        {
            yield return null;
        }
        ReturnAudioSourceToPool(audioSource);
    }

    public void PlaySplitSfx(Vector3 position)
    {
        PlayClipAtPoint(root_split.GetRandomClip(), position, 2.0f, mixer_sfx, 0.0f);
    }

    private void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume, AudioMixerGroup mixer, float spatialBlend = 1.0f)
    {
        var audiosource = GetAudioSource();
        // audiosource.name = $"{clip.name}_oneshot";
        audiosource.transform.parent = this.transform;
        audiosource.clip = clip;
        audiosource.transform.position = position;
        audiosource.volume = volume;
        audiosource.spatialBlend = spatialBlend;
        audiosource.outputAudioMixerGroup = mixer;
        audiosource.Play();
        StartCoroutine(AudioSourceListener(audiosource));
    }

    private void ProcessQueue()
    {
        if (stack.Count < 0)
            return;

        while(stack.Count > 0)
        {
            var entry = stack.Pop();
            if (!processedList.Contains(entry.Hash))
            {
                processedList.Add(entry.Hash);
                //tip/prop collision
                if (CollisionIsOfType(entry, AudioTag.Tip, AudioTag.Prop))
                {
                    PlayClipAtPoint(tip_prop.GetRandomClip(), entry.Position, entry.volume, mixer_roottip);
                }
                else if (CollisionIsOfType(entry, AudioTag.Tip, AudioTag.Default))
                {
                    PlayClipAtPoint(tip_nomove.GetRandomClip(), entry.Position, entry.volume, mixer_roottip);
                }
                else if (CollisionIsOfType(entry, AudioTag.Default, AudioTag.Prop))
                {
                    PlayClipAtPoint(prop_nomove.GetRandomClip(), entry.Position, entry.volume, mixer_general);
                }
                else if (CollisionIsOfType(entry, AudioTag.RootBody, AudioTag.Default))
                {
                    PlayClipAtPoint(rootBody_nomove.GetRandomClip(), entry.Position, entry.volume, mixer_rootbody);
                }
                else if (CollisionIsOfType(entry, AudioTag.RootBody, AudioTag.Prop))
                {
                    PlayClipAtPoint(rootBody_prop.GetRandomClip(), entry.Position, entry.volume, mixer_rootbody);
                }
                else if (CollisionIsOfType(entry, AudioTag.RootBody, AudioTag.Tip))
                {
                    PlayClipAtPoint(rootBody_tip.GetRandomClip(), entry.Position, entry.volume, mixer_roottip);
                }
            }
        }
        processedList.Clear();
    }

    public void QueueSound(AudioTag col1, AudioTag col2, Vector3 position, float volume)
    {
        stack.Push(new AudioCollision(col1, col2, position, volume));
    }
}
