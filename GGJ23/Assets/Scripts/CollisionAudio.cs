using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioTag { None, Tip, Default, Prop, RootBody }

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class CollisionAudio : MonoBehaviour
{
    // private AudioSource audioSource;
    // public LayerMask layerMask;
    public AudioTag audioTag;

    //make a hash from the two objects colliding and the time.
    //that gets sent to the audio manager


    void OnCollisionEnter2D(Collision2D collision)
    {
        // if (collision.relativeVelocity.magnitude > 2);
        if (collision.collider.TryGetComponent<CollisionAudio>(out var otherCollisionAudio))
        {
            AudioManager.Instance.QueueSound(this.audioTag, otherCollisionAudio.audioTag, collision.GetContact(0).point, collision.relativeVelocity.magnitude);
        }
    }

    int MakeCollisionHash(Collision2D collision)
    {
        return -1;
    }

    // public static int MakeHash()
}