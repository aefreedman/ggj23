﻿using System;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Roots
{
    public class PhysicsSteering : Steering
    {
        public float Accel;
        public float Speed;
        public float TimerMin = 0.5f;
        public float TimerMax = 1.5f;
        public float MousePullForce;
        public float MousePullRange = 5f;
        public float MousePullDeadzone = 0.5f;
        public float MaxSpeed = 5.0f;
        public float Friction = 0.05f;

        [SerializeField]
        protected Vector3 _velocity, _desiredVelocity;

        private Rigidbody2D _body;
        private Vector3 _inputDirection;
        private float _timer;
        private Vector3 mousePosition;

        // may need statemachine instead
        private bool _underMouseControl;
        private bool _didInit;

        private void Start()
        {
            Init();
        }

        public void Init()
        {
            if (_didInit) return;
            DirectionAngle = Random.Range(0, 359);
            _body = GetComponent<Rigidbody2D>();
            Accel *= Random.Range(0.8f, 1.2f);
            _didInit = true;
        }

        private void Update()
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0;

            _timer -= Time.deltaTime;
            if (_timer > 0)
                return;
            _timer = Random.Range(TimerMin, TimerMax);

            if (_underMouseControl) return;

            var rand = Random.Range(0, 2);
            if (rand == 0)
                DirectionAngle += 35;
            else
                DirectionAngle += -35;

            _inputDirection = Speed * new Vector3(Mathf.Cos(DirectionAngle * Mathf.Deg2Rad), Mathf.Sin(DirectionAngle * Mathf.Deg2Rad), 0f);
        }

        private void FixedUpdate()
        {
            var dist = Vector2.Distance(mousePosition, transform.position);
            if (dist <= MousePullRange)
            {
                _underMouseControl = true;
                var dir = mousePosition - transform.position;
                // // if (dir.sqrMagnitude > 1.0f)
                // //     dir = dir.normalized;
                // // _body.velocity = dir.normalized * MousePullForce * (1.0f - dist/MousePullRange);
                // _velocity = Vector3.Lerp(
                //     _velocity,
                //     dir.normalized * MousePullForce * (1.0f - dist / MousePullRange),
                //     Mathf.Pow(1.0f - dist / MousePullRange, 2.2f)
                // ); //this mixes in the mouse pull direction. there are a ton of different ways we can do this.

                if (dist > MousePullDeadzone)
                    _inputDirection = dir;

                dir *= Mathf.Pow(1.0f - dist/MousePullRange,2.2f);
                DirectionAngle = Vector2.Angle(Vector2.up, dir);
                // return;
            }
            else
            {
                _underMouseControl = false;
            }

            _desiredVelocity = _inputDirection * MaxSpeed;
            UpdateVelocity();
            _velocity += Physics.gravity * _body.gravityScale * Time.deltaTime;

            _body.velocity = _velocity * (1 - Friction);
        }

        private void OnDisable()
        {
            _body.velocity = Vector2.zero;
        }

        private void UpdateVelocity()
        {
            // contacthandling can go here
            var xAxis = transform.right;
            var yAxis = transform.up;
            var currentX = Vector3.Dot(_velocity, xAxis);
            var currentY = Vector3.Dot(_velocity, yAxis);
            var newX = Mathf.MoveTowards(currentX, _desiredVelocity.x, Accel * Time.deltaTime);
            var newY = Mathf.MoveTowards(currentY, _desiredVelocity.y, Accel * Time.deltaTime);
            _velocity += xAxis * (newX - currentX) + yAxis * (newY - currentY);
        }

        public void OverrideVelocity(Vector2 velocity)
        {
            _velocity = velocity;
            _body.velocity = velocity;
            _desiredVelocity = velocity;
        }

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            if (!enabled)
                return;
            DrawMouseRange();
            DrawMoveDirection();
            DrawTargetDirection();
        }

        private void DrawMouseRange()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(mousePosition, MousePullRange);
        }

        private void DrawMoveDirection()
        {
            Handles.color = Color.gray;
            var pos = transform.position;
            Handles.ArrowHandleCap(0, pos, Quaternion.LookRotation(_velocity), HandleUtility.GetHandleSize(pos), EventType.Repaint);
        }

        private void DrawTargetDirection()
        {
            Handles.color = Color.blue;
            var pos = transform.position;
            Handles.ArrowHandleCap(0, pos, Quaternion.LookRotation(_inputDirection), HandleUtility.GetHandleSize(pos), EventType.Repaint);
        }
#endif
    }
}