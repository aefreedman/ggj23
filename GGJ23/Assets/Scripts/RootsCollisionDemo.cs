using UnityEngine;

namespace Roots
{
    public static class RootHelper
    {
        public static void UpdateColliderFromLineRenderer(ref PolygonCollider2D col, RootLineRenderer root, RootController controller)
        {
            var lr = root.LineRenderer;
            if (lr.positionCount < 2)
                return;

            var points = new Vector3[lr.positionCount];
            lr.GetPositions(points);
            col.pathCount = points.Length - 1;
            for (var i = 0; i < points.Length - 1; i++)
            {
                Vector2 p0 = points[i];
                Vector2 p1 = points[i + 1];

                var i0 = points.Length - 1 - i;
                var i1 = points.Length - 1 - i + 1;
                var p1_hwidth = root.MaxWidth * (i0 / (float)controller.RootLengthLimit) * 0.5f;
                var p0_hwidth = root.MaxWidth * (i1 / (float)controller.RootLengthLimit) * 0.5f;

                var normal = Vector2.Perpendicular(p1 - p0).normalized;

                var v = new Vector2[4];
                v[0] = p0 - normal * p0_hwidth; //bottom left
                v[1] = p0 + normal * p0_hwidth; //top left
                v[2] = p1 + normal * p1_hwidth; //top right
                v[3] = p1 - normal * p1_hwidth; //bottom right

                col.SetPath(i, v);
            }
        }
    }

    public class RootsCollisionDemo : MonoBehaviour
    {
        public RootLineRenderer root;
        public RootController Controller;

        public PolygonCollider2D col;


        private void Start()
        {
            RootHelper.UpdateColliderFromLineRenderer(ref col, root, Controller);
        }

        private void LateUpdate()
        {
            if (root.enabled)
                RootHelper.UpdateColliderFromLineRenderer(ref col, root, Controller);
        }
    }
}