﻿using System;
using System.Collections;
using UnityEngine;

namespace Roots
{
    public class RootSource : MonoBehaviour
    {
        public GameObject Prefab;
        public bool CanSpawn;
        
        private void OnMouseDown()
        {
            if (!CanSpawn) return;
            
            Instantiate(Prefab);
            StartCoroutine(OnSpawn());
        }

        private IEnumerator OnSpawn()
        {
            CanSpawn = false;
            yield return new WaitForEndOfFrame();
            CanSpawn = true;
        }
    }
}