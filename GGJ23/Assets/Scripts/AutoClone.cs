﻿using UnityEngine;

namespace Roots
{
    public class AutoClone : MonoBehaviour
    {
        public float TimeMin;
        public float TimeMax;
        public GameObject Prefab;
        public RootLineRenderer RootLineRenderer;
        public PhysicsSteering Steering;
        public Rigidbody2D Rigidbody;
        public bool CloneFromMiddle;
        private float _timer;

        private void Start()
        {
            _timer = Random.Range(TimeMin, TimeMax);
            RootLineRenderer = GetComponent<RootLineRenderer>();
        }

        private void Update()
        {
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
                return;
            }

            var obj = Instantiate(Prefab);
            var middle = RootLineRenderer.GetMiddle();
            obj.transform.SetPositionAndRotation(CloneFromMiddle ? middle : transform.position, Quaternion.identity);


            if (!CloneFromMiddle)
                Steering.OverrideVelocity(Quaternion.AngleAxis(-90f, Vector3.forward) * Rigidbody.velocity.normalized * (Steering.MaxSpeed));
            var ps = obj.GetComponent<PhysicsSteering>();
            ps.Init();
            ps.OverrideVelocity(Quaternion.AngleAxis(90f, Vector3.forward) * Rigidbody.velocity.normalized * (ps.MaxSpeed));

            RootLineRenderer.AddCurrentPosition();
            var clone = obj.GetComponent<AutoClone>();
            if (clone)
            {
                clone.TimeMax = clone.TimeMax * 4;
                clone.TimeMin = clone.TimeMin * 4;
            }

            var lr = obj.GetComponent<LineRenderer>();
            lr.sortingOrder++;
            // lr.startWidth = 0.2f;
            // lr.widthCurve.keys[0].value = 0.5f;

            _timer = Random.Range(TimeMin, TimeMax);
            enabled = false;
        }
    }
}