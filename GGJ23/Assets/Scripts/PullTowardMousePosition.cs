﻿using UnityEngine;

namespace Roots
{
    public class PullTowardMousePosition : MonoBehaviour
    {
        public Steering Steering;

        public float Strength;

        private void Update()
        {
            var mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouse.z = 0;
            var dir = (mouse - transform.position).normalized;
            // var pull = new Vector3(Mathf.Cos(Steering.DirectionAngle * Mathf.Deg2Rad), Mathf.Sin(Steering.DirectionAngle * Mathf.Deg2Rad), 0f);
            // Steering.transform.position += dir * Strength / 1000000;
        }
    }
}