using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    public class AutoAddLineRenderer : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            if (RootsCollisionManager.Instance == null)
                return;

            var lr = this.GetComponent<RootLineRenderer>();
            try
            {
                RootsCollisionManager.Instance.AddLineRenderer(lr);
            }
            catch (ArgumentException e)
            {
                Debug.LogWarning($"Ignoring exception: {e.Message}");
                // Console.WriteLine(e);
                // throw;
            }
        }
    }
}