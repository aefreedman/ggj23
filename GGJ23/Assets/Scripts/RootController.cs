﻿using System.Collections;
using UnityEngine;

namespace Roots
{
    public class RootController : MonoBehaviour
    {
        public RootLineRenderer RootLineRenderer;
        public Steering Steering;
        public GameObject FlowerPrefab;
        public SpriteRenderer Renderer;

        public bool HasLengthLimit;
        public int RootLengthLimit;

        private Vector3 _mousePosition;

        private float _mouseDist;
        private float _alphaPulse;

        private Coroutine _lightCoroutine;

        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.tag)
            {
                case "Water":
                    HitWater();
                    break;
                case "Light":
                    _lightCoroutine ??= StartCoroutine(LightCoroutine());
                    break;
            }
        }

        public void HitWater()
        {
            // do something
            Stop();
            SpawnFlowers();
        }

        public void HitLight()
        {
            // same as water but uses randomness to allow the roots to go into the light a bit
            var random = Random.Range(0, 6);
            if (random >= 2)
                return;
            Stop();
            SpawnFlowers();
        }

        private IEnumerator LightCoroutine()
        {
            var didHit = false;
            while (!didHit)
            {
                var random = Random.Range(0, 6);
                if (random >= 1)
                {
                    yield return new WaitForSeconds(0.5f);
                }
                else
                {
                    didHit = true;
                }
            }

            Stop();
            SpawnFlowers();
        }

        private void Update()
        {
            if (HasLengthLimit && RootLineRenderer.LineRenderer.positionCount >= RootLengthLimit)
            {
                Stop();
                return;
            }

            _mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            // click range is currently 2 and pull range is 5 these are all set separately and we should convert it to a shared scriptable obj or struct
            _mouseDist = Vector2.Distance(_mousePosition, transform.position);
            Renderer.color = _mouseDist switch
            {
                <= 2f => new Color(0f, 1f, 0f, 0.5f),
                > 2f and < 5f => new Color(1f, 1f, 1f, (_mouseDist - 2 / 5f) / 2),
                _ => Color.clear
            };
        }

        public void Stop()
        {
            RootLineRenderer.enabled = false;
            Steering.enabled = false;
            enabled = false;
            GetComponent<CloneOnClick>().enabled = false;
            Renderer.enabled = false;
        }

        public void SpawnFlowers()
        {
            if (RootLineRenderer.GetPositions == null) return;
            foreach (var position in RootLineRenderer.GetPositions)
            {
                if (Random.Range(0, 10) >= 5)
                    Instantiate(FlowerPrefab, position, Quaternion.Euler(0, 0, Random.Range(-26.0f, 26.0f)));
            }
        }
    }
}