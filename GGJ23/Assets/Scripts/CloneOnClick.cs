﻿using System;
using System.Collections;
using UnityEngine;

namespace Roots
{
    public class CloneOnClick : MonoBehaviour
    {
        public RootLineRenderer rootLineRenderer;
        public Rigidbody2D Rigidbody;
        public PhysicsSteering Steering;
        public GameObject Prefab;
        private AudioManager _audioManager;
        private bool _canSpawn;

        [Tooltip("Amount to multiply velocity during split to make the split visible")]
        public float SplitForce = 2f;

        private void Start()
        {
            _audioManager = AudioManager.Instance;
            StartCoroutine(SpawnDelay());
        }

        private IEnumerator SpawnDelay()
        {
            // wait two frames to prevent cloning on intantiation
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            _canSpawn = true;
        }

        public void Split()
        {
            if (!_canSpawn) return;
            var obj = Instantiate(Prefab);
            obj.transform.position = transform.position;
            rootLineRenderer.AddCurrentPosition();

            Steering.OverrideVelocity(Quaternion.AngleAxis(-90f, Vector3.forward) * Rigidbody.velocity.normalized * (Steering.MaxSpeed));
            var ps = obj.GetComponent<PhysicsSteering>();
            ps.Init();
            ps.OverrideVelocity( Quaternion.AngleAxis(90f, Vector3.forward) * Rigidbody.velocity.normalized * (ps.MaxSpeed));

            if (_audioManager)
                _audioManager.PlaySplitSfx(transform.position);
        }

        private void OnDisable()
        {
            _canSpawn = false;
        }
    }
}