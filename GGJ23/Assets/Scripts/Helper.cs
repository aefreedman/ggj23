using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper
{
    public static int CombineHashCodes(int h1, int h2)
    {
        return (((h1 << 5) + h1) ^ h2);
    }
}
