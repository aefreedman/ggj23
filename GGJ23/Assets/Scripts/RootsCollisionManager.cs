using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using UnityEngine;

namespace Roots
{
    public class RootsCollisionManager : MonoBehaviour
    {
        public static RootsCollisionManager Instance;
        public EdgeCollider2D RootSegmentPhysicsPrefab;

        private List<LineRenderer> lineRenderers;
        private List<RootLineRenderer> _rootLineRenderers;
        private Dictionary<LineRenderer, EdgeCollider2D> lookup;

        
        void Awake()
        {
            Application.targetFrameRate = 60;
            Instance = this;
            lineRenderers = new List<LineRenderer>();
            _rootLineRenderers = new List<RootLineRenderer>();
            lookup = new Dictionary<LineRenderer, EdgeCollider2D>();
        }

        void Update()
        {
            for (var index = 0; index < lineRenderers.Count; index++)
            {
                var lr = lineRenderers[index];
                if (!lr.enabled)
                    return;

                var p0 = lr.GetPosition(lr.positionCount - 2);
                var p1 = lr.GetPosition(lr.positionCount - 1);
                var edge = lookup[lr];
                Vector2 [] points = new Vector2[2] {Vector2.zero, p1-p0};
                edge.transform.position = p0;
                edge.points = points;
            }
        }

        //only called for linerenderers that have already been added to the dictionary.
        //there's no checks for speed's sake so this is unsafe! xd
        public void AddNewSegment(LineRenderer lr, Vector2 p0, Vector2 p1)
        {
            var edgeCollider = Instantiate(RootSegmentPhysicsPrefab);
            edgeCollider.transform.position = p0;
            Vector2 [] points = new Vector2[2] {Vector2.zero, p1-p0};
            edgeCollider.points = points;
            lookup[lr].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static; //this should theoretically help performance.
            lookup[lr] = edgeCollider;
        }

        public void AddLineRenderer(RootLineRenderer rlr)
        {
            var lr = rlr.LineRenderer;
            lineRenderers.Add(rlr.LineRenderer);
            _rootLineRenderers.Add(rlr);
            var points = new Vector3[lr.positionCount];
            lr.GetPositions(points);

            var edgeCollider = Instantiate(RootSegmentPhysicsPrefab);
            edgeCollider.transform.position = points[0];
            Vector2 [] vpoints = new Vector2[2] {Vector2.zero, points[1]-points[0]};
            edgeCollider.points = vpoints;
            lookup.Add(lr, edgeCollider);
        }

    }
}
