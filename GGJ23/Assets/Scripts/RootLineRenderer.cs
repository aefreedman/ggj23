using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    [RequireComponent(typeof(Steering)), RequireComponent(typeof(LineRenderer))]
    public class RootLineRenderer : MonoBehaviour
    {
        public RootController Controller;
        public Steering Steering;
        public LineRenderer LineRenderer;
        public float PositionGap;

        public float MaxWidth = 3f;

        // Start is called before the first frame update

        private Vector3 _lastPos;

        private List<Vector3> _positionCache;
        private bool _didInitialize;

        private void Start()
        {
            if (!_didInitialize)
                Init();
        }

        private void Init()
        {
            // var p0 = transform.position + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0)* 0.001f;
            // var p1 = p0 + new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0)* 0.001f; //p1 is going to become the 
            var p0 = transform.position;
            var p1 = p0; // + new Vector3(1.0f, 0, 0); //p1 is going to become the 
            _lastPos = p0;
            _positionCache = new List<Vector3> { p0, p1 };
            LineRenderer.positionCount = 2;
            LineRenderer.SetPositions(_positionCache.ToArray());
            LineRenderer.startWidth = 0.1f;
            _didInitialize = true;
        }

        // Update is called once per frame
        private void Update()
        {
            var addedNewPositionThisFrame = false;
            var position = transform.position;

            if (Vector3.SqrMagnitude(position - _lastPos) > PositionGap * PositionGap)
            {
                addedNewPositionThisFrame = true;
                _positionCache.Add(position);
                LineRenderer.positionCount = _positionCache.Count;
                LineRenderer.startWidth = MaxWidth * LineRenderer.positionCount / Controller.RootLengthLimit;
                LineRenderer.SetPositions(_positionCache.ToArray());
            }

            _positionCache[^1] = position;
            LineRenderer.SetPositions(_positionCache.ToArray());

            if (addedNewPositionThisFrame)
            {
                if (RootsCollisionManager.Instance != null)
                    RootsCollisionManager.Instance.AddNewSegment(LineRenderer, _lastPos, position);
                _lastPos = position;
            }
        }

        public void AddCurrentPosition()
        {
            if (!_didInitialize)
                Init();
            var position = transform.position;
            _lastPos = position;
            _positionCache.Add(position);
            LineRenderer.positionCount = _positionCache.Count;
            LineRenderer.startWidth = MaxWidth * LineRenderer.positionCount / Controller.RootLengthLimit;
            LineRenderer.SetPositions(_positionCache.ToArray());
            if (RootsCollisionManager.Instance != null)
                RootsCollisionManager.Instance.AddNewSegment(LineRenderer, _positionCache[^2], _positionCache[^1]);
        }

        public Vector3 GetMiddle()
        {
            if (_positionCache == null || _positionCache.Count < 2)
                return transform.position;
            return _positionCache[_positionCache.Count / 2];
        }

        public List<Vector3> GetPositions => _positionCache;
    }
}