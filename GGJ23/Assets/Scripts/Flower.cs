﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Roots
{
    public class Flower : MonoBehaviour
    {
        public float StartSizeMin, StartSizeMax, EndSizeMin,EndSizeMax;
        public float OffsetMin, OffsetMax;
        public float RotationSpeed;
        private int _rotationDirection;
        public float SpinTime = 0.1f;
        private float _timer;
        private float _scaleTarget;

        private void Start()
        {
            transform.Translate(new Vector3(Random.Range(OffsetMin, OffsetMax), Random.Range(OffsetMin, OffsetMax)));
            var scale = Random.Range(StartSizeMin, StartSizeMax);
            transform.localScale = new Vector3(scale, scale, 1);
            gameObject.isStatic = true;
            gameObject.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, .2f, 0f, .4f, 1f, 1f); ;
            // _scaleTarget = Random.Range(EndSizeMin, EndSizeMax);
            // StartCoroutine(Rotate());
            // StartCoroutine(Grow());
        }

        // private void Update()
        // {
        //     var done = true;
        //     if (_timer < SpinTime)
        //     {
        //         _timer += Time.deltaTime;
        //         transform.Rotate(0, 0, RotationSpeed * Time.deltaTime);
        //         done = false;
        //     }
        //     if (transform.localScale.magnitude < _scaleTarget)
        //     {
        //         transform.localScale = new Vector3(transform.localScale.x * 1.1f * Time.deltaTime, transform.localScale.y * 1.1f * Time.deltaTime, 1);
        //         done = false;
        //     }
        //
        //     if (done) enabled = false;
        // }

        private IEnumerator Rotate()
        {
            while (_timer < SpinTime)
            {
                _timer += Time.deltaTime;
                transform.Rotate(0, 0, RotationSpeed * Time.deltaTime);
                yield return null;
            }
            yield return null;
        }

        private IEnumerator Grow()
        {
            while (transform.localScale.magnitude < _scaleTarget)
            {
                transform.localScale *= (2 * Time.deltaTime);
                yield return null;
            }
            yield return null;
        }
    }
}