﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Roots
{
    public class GameController : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetButtonDown("Debug Reset"))
                SceneManager.LoadScene("SampleScene");
        }
    }
}