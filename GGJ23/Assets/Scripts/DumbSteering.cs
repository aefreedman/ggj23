﻿using UnityEngine;

namespace Roots
{
    public class DumbSteering : Steering
    {
        public float Speed;
        public float TimerMin = 0.5f;
        public float TimerMax = 1.5f;
        private float _timer;

        private void Start()
        {
            DirectionAngle = Random.Range(0, 359);
        }

        private void Update()
        {
            _timer -= Time.deltaTime;
            if (_timer <= 0)
            {
                _timer = Random.Range(TimerMin, TimerMax);
                var rand = Random.Range(0, 2);
                if (rand == 0)
                    DirectionAngle += 35;
                else
                    DirectionAngle += -35;
            }

            transform.position += Speed * new Vector3(Mathf.Cos(DirectionAngle * Mathf.Deg2Rad), Mathf.Sin(DirectionAngle * Mathf.Deg2Rad), 0f);
        }
    }
}