using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Roots
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpirteAnimationPlayer : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;
        [SerializeReference]
        public Sprite[] sprites;

        public float FPS = 3.0f;

        void Awake()
        {
            StartCoroutine(PlayAnimOneShot());
        }

        IEnumerator PlayAnimOneShot()
        {
            float timePerFrame = sprites.Length/FPS;
            float elapsed = 0.0f;
            int itr = 0;
            spriteRenderer.sprite = sprites[itr];
            while (itr < sprites.Length)
            {
                elapsed += Time.deltaTime;
                if (elapsed >= timePerFrame)
                {
                    elapsed = 0.0f;
                    itr++;
                    if (itr < sprites.Length)
                        spriteRenderer.sprite = sprites[itr++];
                }
                yield return null;
            }
        }

    }
}
