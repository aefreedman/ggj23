﻿using UnityEngine;

namespace Roots
{
    public class CameraController : MonoBehaviour
    {
        public float Speed;

        private void Update()
        {
            transform.Translate(new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * (Speed * Time.deltaTime));
        }
    }
}