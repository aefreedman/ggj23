using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Roots
{
    public class ResourceSprite : MonoBehaviour
    {
        private SpriteRenderer sr;
        private float c_alpha;
        private float t_alpha;
        public float min_alpha;
        public float max_alpha;
        public float lerp_t;

        // Start is called before the first frame update
        void Start()
        {
            sr = GetComponent<SpriteRenderer>();
            c_alpha = Random.Range(min_alpha, max_alpha);
            t_alpha = Random.Range(min_alpha, max_alpha);
        }

        // Update is called once per frame
        void Update()
        {
            if (Mathf.Abs(c_alpha - t_alpha) > .002)
            {
                c_alpha = Mathf.Lerp(c_alpha, t_alpha, lerp_t);
            } else
            {
                t_alpha = Random.Range(min_alpha, max_alpha);
            }
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, c_alpha);
        
        }
    }
}
