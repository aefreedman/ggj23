using System.Collections;
using UnityEngine;

namespace Roots
{
    public class MouseController : MonoBehaviour
    {
        public float interactionRadius = 2.0f;
        public LayerMask interactableLayers;
        private Coroutine _spawnCoroutine;

        private Vector3 mousePosition;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Confined;
        }


        private void Update()
        {
            if (Camera.main)
                mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            if (Input.GetMouseButtonDown(0) && _spawnCoroutine == null)
                _spawnCoroutine = StartCoroutine(SpawnCoroutine());
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(mousePosition, interactionRadius);
        }

        private IEnumerator SpawnCoroutine()
        {
            //does a raycast against only RootTip Layer
            var _results = Physics2D.OverlapCircleAll(mousePosition, interactionRadius, interactableLayers);

            for (var i = 0; i < _results.Length; i++)
            {
                if (i != 0)
                    continue;
                _results[i].GetComponent<CloneOnClick>().Split();
            }

            yield return null;
            _spawnCoroutine = null;
        }
    }
}